import "./App.css";
import { useState } from "react";

function App() {
  let array = ["australia", "india", "england", "new zealand", "pakistan"];
  let [box1, setBox1] = useState(array);
  let [box2, setBox2] = useState([]);
  let transfer1 = () => {
    setBox2((prev) => [...prev, ...tmp2]);
    setBox1(box1.filter((each) => !tmp2.includes(each)));
  };
  let tmp2 = [],
    tmp1 = [];
  let transfer2 = () => {
    setBox1((prev) => [...prev, ...tmp1]);
    setBox2(box2.filter((each) => !tmp1.includes(each)));
  };
  let allRight = () => {
    setBox2(array);
    setBox1([]);
  };
  let allLeft = () => {
    setBox1(array);
    setBox2([]);
  };
  return (
    <>
      <div
        style={{ border: "1px solid black", width: "150px", height: "150px" }}
      >
        {box1.map((each) => {
          return (
            <div key={each}>
              <input
                type="checkbox"
                value={each}
                id={each}
                onChange={(e) => {
                  if (e.target.checked === true) {
                    if (!tmp2.includes(e.target.value)) {
                      tmp2.push(e.target.value);
                    }
                  } else if (e.target.checked === false) {
                    if (tmp2.includes(e.target.value)) {
                      tmp2 = tmp2.filter((el) => el !== e.target.value);
                    }
                  }
                }}
              />
              <label htmlFor={each}>{each}</label>
            </div>
          );
        })}
      </div>
      <div
        style={{ border: "1px solid black", width: "150px", height: "150px" }}
      >
        {box2.map((each) => {
          return (
            <div key={each}>
              <input
                type="checkbox"
                value={each}
                id={each}
                onChange={(e) => {
                  if (e.target.checked === true) {
                    if (!tmp1.includes(e.target.value)) {
                      tmp1.push(e.target.value);
                    }
                  } else if (e.target.checked === false) {
                    if (tmp1.includes(e.target.value)) {
                      tmp1 = tmp1.filter((el) => el !== e.target.value);
                    }
                  }
                }}
              />
              <label htmlFor={each}>{each}</label>
            </div>
          );
        })}
      </div>
      <button onClick={transfer1}>send to down</button>
      <button onClick={transfer2}>send to up</button>
      <button onClick={allRight}>all to down</button>
      <button onClick={allLeft}>all to up</button>
    </>
  );
}

export default App;
